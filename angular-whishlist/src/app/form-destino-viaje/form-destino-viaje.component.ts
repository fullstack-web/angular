import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>
  fg: FormGroup;
  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ["", Validators.required],
      url: ["", Validators.required]
    })
  }
  guardar2(nombre: string, url: string) {
    let d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d)
    console.log("Evento de creacion de formulario emitido en ts form")
  }

  ngOnInit(): void {
  }

}
