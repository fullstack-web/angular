import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];
  constructor() {
    this.destinos = []
  }

  ngOnInit(): void {
  }
  guardar(destino: DestinoViaje) {
    this.destinos.push(destino)
  }

  /** Llamada cuando un destino emite un evento **/
  elegido(d: DestinoViaje) {
    this.destinos.forEach(function(x) {
      x.setSeleccionada(false);
    })
    d.setSeleccionada(true)
  }


}
