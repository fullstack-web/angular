import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';


@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @HostBinding("attr.class") cssClass = "col-md-4"
  @Output() listenerBotonFavorito: EventEmitter<DestinoViaje>
  constructor() {
    this.listenerBotonFavorito = new EventEmitter();
  }

  ngOnInit(): void {
  }
  /* Llamada cuando se da click en un boton */
  marcarFavorito() {
    this.listenerBotonFavorito.emit(this.destino);
    return false;
  }

}
