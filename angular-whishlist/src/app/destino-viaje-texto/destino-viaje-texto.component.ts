import { Component, OnInit, Input } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje-texto',
  templateUrl: './destino-viaje-texto.component.html',
  styleUrls: ['./destino-viaje-texto.component.css']
})
export class DestinoViajeTextoComponent implements OnInit {
  @Input() destino: DestinoViaje;
  constructor() { }

  ngOnInit(): void {
  }

}
