import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoViajeTextoComponent } from './destino-viaje-texto.component';

describe('DestinoViajeTextoComponent', () => {
  let component: DestinoViajeTextoComponent;
  let fixture: ComponentFixture<DestinoViajeTextoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DestinoViajeTextoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoViajeTextoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
