export class DestinoViaje {
  nombre: string;
  imagenUrl: string;
  seleccionada: boolean;
  servicios: string[];

  constructor(nombre: string, imagenUrl: string) {
    this.nombre = nombre;
    this.imagenUrl = imagenUrl;
  }
  setSeleccionada(seleccion: boolean) {
    this.seleccionada = seleccion;
  }
  getSeleccionada(): boolean {
    return this.seleccionada;
  }
}
